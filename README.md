# README #

The runnable version of this code can be found at

	[https://embed.plnkr.co/GWmJS0EBa1i4wGaUv74r/](Link URL)

The 3 questions are:

	1. Do you rent or pay mortgage?

		* Capture monthly rent or amount remaining on mortgage

	2. Do you have children, if so how many?
	3. How much outstanding debt do you have?

Once the customer has completed the journey they will be presented with their calculated cover amount and a summary of the options they selected.

	Cover Amount Calculation = (Mortgage or Rent*12*20) + (no. of Children * 50,000) + Debt


### How do I get set up? ###

	git clone https://ariefshah@bitbucket.org/ariefshah/insurancecalculator.git

Once the code is downloaded, please navigate to the code folder, and run 

	npm install
	(assuming node is installed)

once all the frameworks are installed please run,

	bower update

Then to start the app, please run 

	gulp start

It will open two browser windows

	 one the actual app and 
	 the other Protractor test framework.

All the code files are under 'app' directory, one running gulp task it will create a wwwroot folder where all the minified css, minified javascript and minified html will be copied and this will be root folder where all the requests will be served from.

Calculatrice d’assurance est une application développée afin de calculer et de couvrir le montant d’assurance. La demande d’assurance ci-dessus est conçue pour vous montrer l’éventail des montants que vous allez servir. http://azurexpat.com/ est une société d’assurance internationale qui utilise ces calculatrices d’assurance pour la gestion financière. Plusieurs compagnies d’assurance internationales utilisent ces calculatrices pour couvrir le montant à servir.

###Assumption ###
The following statment is not clear and has been omitted

	The cover amount range allowed is between $50,000 - $1,000,000