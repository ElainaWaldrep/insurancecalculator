/// <binding AfterBuild='build-assets, build, copy-bower_fonts, usemin' Clean='clean' />
var gulp = require("gulp"),
    usemin = require('gulp-usemin'),
    wrap = require('gulp-wrap'),
    connect = require('gulp-connect'),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    project = require("./project.json"),
    watch = require('gulp-watch'),
    minifyCss = require('gulp-minify-css'),
    minifyJs = require('gulp-uglify'),
    less  = require('gulp-less'),
    rename = require('gulp-rename'),
    minifyHTML = require('gulp-minify-html'),
    rev = require('gulp-rev'),
     os = require('os'),
    open = require('gulp-open'),
    webserver = require('gulp-webserver'),
        connect = require('gulp-connect');;

var paths = {
    webroot: "./" //+ project.webroot + "/"
   };

paths.js = paths.webroot + "bower_components/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

paths.scripts = paths.webroot + 'app/**/*.js',
paths.styles = paths.webroot + 'app/**/*.css',
paths.images = paths.webroot + 'img/**/*.*',
paths.templates = paths.webroot + 'app/template/*.html',
paths.index = paths.webroot + 'app/index.html',
paths.bower_fonts = paths.webroot + 'app/fonts/*.{ttf,woff,eof,svg}'
/**
 * Handle bower components from index
 */
gulp.task('use-min', function () {
    return gulp.src(paths.index)
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            /*css: [minifyCss({keepSpecialComments: 0}), 'concat'],*/
            css: [rev()]
        }))
        .pipe(gulp.dest('wwwroot'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-bower_fonts']);

gulp.task('copy-bower_fonts', function () {
    return gulp.src(paths.bower_fonts)
        .pipe(rename({
            dirname: '/fonts'
        }))
        .pipe(gulp.dest('wwwroot'));
});

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

/**
 * Handle custom files
 */
gulp.task('build-custom', ['custom-js', 'custom-less', 'custom-templates', 'cssMain']);

gulp.task('custom-js', function () {
    return gulp.src(paths.scripts)
       // .pipe(minifyJs())
        .pipe(concat('custom.min.js'))
        .pipe(gulp.dest('wwwroot/js'));
});

gulp.task('custom-less', function () {
    return gulp.src(paths.styles)
        .pipe(less())
        .pipe(gulp.dest('wwwroot/css'));
});

gulp.task('custom-cssMain', function () {
    return gulp.src(paths.mainstyles)
        .pipe(minifyCss())
        .pipe(concat('main.min.css'))
        .pipe(gulp.dest('wwwroot/css'));
});
gulp.task('custom-templates', function () {
    return gulp.src(paths.templates)
        .pipe(minifyHTML())
        .pipe(gulp.dest('wwwroot/template'));
});


/**
 * Gulp tasks
 */


gulp.task('connect', function () {
    connect.server({
        root: 'wwwroot',
        livereload: true
    });
});

gulp.task('html', function () {
    gulp.src('./app/*.html')
      .pipe(connect.reload());
});

gulp.task('watch', function () {
    gulp.watch(['./app/*.html'], ['html']);
});

gulp.task('default', ['connect', 'watch']);


gulp.task('run', function () {
    var options = {
        uri: 'http://localhost:8080',
        app: 'chrome'
    };
    gulp.src(__filename)
    .pipe(open(options));
});

gulp.task('tests', function () {
    connect.server({
        port: 7776,
        root: 'SpecRunner',
        livereload: true
    });
    
});

gulp.task('runTest', function () {
    var options = {
        uri: 'http://localhost:7776',
        app: 'chrome'
    };
    gulp.src(__filename)
    .pipe(open(options));
});

/**
 * Gulp tasks
 */

gulp.task('start', ['custom-templates', 'use-min', 'copy-bower_fonts'
    , 'default', 'run', 'tests', 'runTest'])