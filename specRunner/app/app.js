// create our angular app and inject ngAnimate and ui-router 
// =============================================================================
angular.module('formApp', ['ngAnimate', 'ui.router'])

// configuring our routes 
// =============================================================================
.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

        // route to show our basic form (/form)
        .state('form', {
            url: '/form',
            templateUrl: 'template/main.html',
            controller: 'formController'
        })

        // nested states 
        // each of these sections will have their own view
        // url will be nested (/form/profile)
        .state('form.profile', {
            url: '/profile',
            templateUrl: 'template/form-mortgageOrRent.html'
        })

        // url will be /form/interests
        .state('form.rent', {
            url: '/rent',
            templateUrl: 'template/form-rent.html'
        })

        // url will be /form/payment
        .state('form.children', {
            url: '/children',
            templateUrl: 'template/form-children.html'
        })

          // url will be /form/payment
        .state('form.noOfchildren', {
            url: '/noOfchildren',
            templateUrl: 'template/form-childrenNo.html'
        })

          // url will be /form/payment
        .state('form.debt', {
            url: '/debt',
            templateUrl: 'template/form-debt.html'
        })
         // url will be /form/payment
        .state('form.result', {
            url: '/result',
            templateUrl: 'template/form-result.html'
        });
    // catch all route
    // send users to the form page 
    $urlRouterProvider.otherwise('/form/profile');
})

// our controller for the form
// =============================================================================
.controller('formController', function ($scope) {

    // we will store all of our form data in this object
    $scope.formData = {};
    $scope.formData.steps = ['','','','',''];
    $scope.formData.balance = 0;
    $scope.formData.childrenCount = 0;
    if ($scope.formData.mortagePay === true) {
        $scope.formData.type = 'mortgage';
      
    }
    else {
        $scope.formData.type = 'rent';
        }


    // function to process the form
    $scope.processForm = function () {

        // Cover Amount Calculation = (Mortgage or Rent*12*20) + (no. of Children * 50,000) + Debt
        if ($scope.formData.mortagePay === true) {
            $scope.formData.balance = $scope.formData.mortgagePay;
        }
        else {
            $scope.formData.balance = $scope.formData.mortgagePay * 12 * 20;
        }

        $scope.formData.coverAmountCalculation = $scope.formData.balance + $scope.formData.childrenCount * 50000
                            + $scope.formData.debt;


    };

});

